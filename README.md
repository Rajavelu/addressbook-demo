# README #

### Description ###
This is an application which can determine changes in address book (which contact(s) and which fields(s) has been changed since the application was last opened)

### Device support ###

* Full support for all iPhone screen sizes
* Portrait orientation

### Language ###
Objective-C

### External Libraries & Dependencies ###

* FMDB for Sqlite 
* DejalActivityView