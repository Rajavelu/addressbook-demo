//
//  AppDelegate.h
//  AddressBook
//
//  Created by Rajavelu Chandrasekaran on 08/09/15.
//  Copyright (c) 2015 Rajavelu Chandrasekaran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

