//
//  ChangesViewController.h
//  AddressBook
//
//  Created by Rajavelu Chandrasekaran on 09/09/15.
//  Copyright (c) 2015 Rajavelu Chandrasekaran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangesViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *changesTable;
@property (strong, nonatomic) NSMutableArray *changesArray;

@end
