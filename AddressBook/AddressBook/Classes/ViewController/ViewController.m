//
//  ViewController.m
//  AddressBook
//
//  Created by Rajavelu Chandrasekaran on 08/09/15.
//  Copyright (c) 2015 Rajavelu Chandrasekaran. All rights reserved.
//

#import "ViewController.h"
#import <AddressBook/AddressBook.h>
#import "DejalActivityView.h"
#import "ChangesViewController.h"
#import "Contact.h"
#import "FMDB.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITableView *contactsTable;
@property (weak, nonatomic) IBOutlet UIButton *checkChangesButton;

@property (strong, nonatomic) NSMutableArray *contactsArray;
@property (strong, nonatomic) NSMutableArray *changesArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib
    
    self.title = @"All Contacts";
    [self createDatabaseTable];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadContactsFromDevice];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation delegates
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    if ([identifier isEqualToString:@"ChangesViewController"]) {
        if (self.changesArray.count == 0) {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"No changes" message:@"Please change one or more contacts and try again" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alertView show];
            return NO;
        }
    }
    
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"ChangesViewController"]) {
        ChangesViewController *changesVC = segue.destinationViewController;
        changesVC.changesArray = self.changesArray;
    }
}


#pragma mark - Private methods
-(void)reInitilaizeSetup {
    if (self.contactsArray == nil) {
        self.contactsArray = [[NSMutableArray alloc]init];
    }
    [self.contactsArray removeAllObjects];
    
    if (self.changesArray == nil) {
        self.changesArray = [[NSMutableArray alloc]init];
    }
    [self.changesArray removeAllObjects];
    
    [self.checkChangesButton setEnabled:NO];
}


#pragma mark - Tableview delegates
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.contactsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    
    Contact *contact = [self.contactsArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = contact.name;
    cell.detailTextLabel.text = contact.number;
    
    return cell;
}

#pragma mark - AddressBook methods
-(void) loadContactsFromDevice {
    // Request authorization to Address Book
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            if (granted) {
                // First time access has been granted, add the contact
                [self reInitilaizeSetup];
                [self fetchContacts];
            } else {
                // User denied access
                // Display an alert telling user the contact could not be added
            }
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        // The user has previously given access, add the contact
        [self reInitilaizeSetup];
        [self fetchContacts];
    }
    else {
        // The user has previously denied access
        // Send an alert telling user to change privacy setting in settings app
    }
}


- (void)fetchContacts {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [DejalBezelActivityView activityViewForView:self.view withLabel:@"Sync is in progress, please wait." width:220];
    });

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        CFErrorRef *error = NULL;
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
        CFIndex numberOfPeople = ABAddressBookGetPersonCount(addressBook);
        
        for(int i = 0; i < numberOfPeople; i++) {
            
            ABRecordRef person = CFArrayGetValueAtIndex( allPeople, i );
            
            Contact *contact = [[Contact alloc] init];
            contact.contactId = ABRecordGetRecordID(person);
            
            //get name
            NSString *firstName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonFirstNameProperty));
            NSString *lastName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonLastNameProperty));
            
            if (firstName != nil && lastName !=nil) {
                contact.name = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
            }else if (firstName !=nil && lastName==nil) {
                contact.name = firstName;
            }else if (firstName == nil && lastName!=nil) {
                contact.name = lastName;
            }else {
                contact.name = @"Unknown";
            }
            
            //get phone Numbers
            NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
            ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);
            
            for(CFIndex i=0; i<ABMultiValueGetCount(multiPhones); i++) {
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                NSString *phoneNumber = CFBridgingRelease(phoneNumberRef);
                if (phoneNumber != nil)[phoneNumbers addObject:phoneNumber];
            }
            
            if (phoneNumbers.count > 0) {
                contact.number = [phoneNumbers componentsJoinedByString:@" | "];
            }else {
                contact.number = @"";
            }
            
            [self.contactsArray addObject:contact];
            [self compareContactFromDatabase:contact];
        }

        // update UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            [DejalBezelActivityView currentActivityView].activityLabel.text = @"Sync is done";
            [DejalBezelActivityView removeViewAnimated:YES];
            
            [self.contactsTable reloadData];
            [self.checkChangesButton setEnabled:YES];
        });
    });
}


#pragma mark - Database methods
-(NSString *)getDataBasePath {
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"Contacts.sqlite"];

    return dbPath;
}

-(void)createDatabaseTable {
    FMDatabase *database = [FMDatabase databaseWithPath:[self getDataBasePath]];
    [database open];
    [database executeUpdate:@"CREATE TABLE contacts (id INTEGER  PRIMARY KEY DEFAULT NULL,name TEXT DEFAULT NULL,number TEXT DEFAULT NULL)"];
    [database close];
}

-(void)insertContactIntoDatabase:(Contact*)contact {
    FMDatabase *database = [FMDatabase databaseWithPath:[self getDataBasePath]];
    [database open];
    [database executeUpdate:@"INSERT INTO contacts (id, name, number) VALUES (?, ?, ?)", [NSNumber numberWithInt:contact.contactId],[NSString stringWithFormat:@"%@",contact.name],[NSString stringWithFormat:@"%@",contact.number], nil];
    [database close];
}

-(void)updateContactInDatabase:(Contact*)contact {
    FMDatabase *database = [FMDatabase databaseWithPath:[self getDataBasePath]];
    [database open];
    [database executeUpdate:@"UPDATE contacts set name='%@' and number='%@' WHERE id='%d' ", contact.name, contact.number, contact.contactId, nil];
    [database close];
}

-(void)compareContactFromDatabase:(Contact*)contact{
        
    FMDatabase *database = [FMDatabase databaseWithPath:[self getDataBasePath]];
    [database open];
    
    FMResultSet *results = [database executeQuery:@"SELECT * FROM contacts WHERE id = ? ",[NSNumber numberWithInt:contact.contactId]];

    while([results next]) {
        
        NSString *resultName = [results stringForColumn:@"name"];
        NSString *resultNumber = [results stringForColumn:@"number"];
        
        if ([contact.name isEqualToString:resultName] && [contact.number isEqualToString:resultNumber]) {
            contact.changeDescription = @"";
        }
        else if (![contact.name isEqualToString:resultName] && [contact.number isEqualToString:resultNumber]) {
            contact.changeDescription = @"Name changed";
            [self.changesArray addObject:contact];
        }
        else if ([contact.name isEqualToString:resultName] && ![contact.number isEqualToString:resultNumber]) {
            contact.changeDescription = @"Number changed";
            [self.changesArray addObject:contact];
        }
        else {
            contact.changeDescription = @"Name and number changed";
            [self.changesArray addObject:contact];
        }
        
    }
   
    NSUInteger count = [database intForQuery:@"SELECT * FROM contacts WHERE id = ? ",[NSNumber numberWithInt:contact.contactId]];
    [database close];
    
    if (count == 0) {
        [self insertContactIntoDatabase:contact];
    }else {
        [self updateContactInDatabase:contact];
    }
}


@end
