//
//  Contact.h
//  AddressBook
//
//  Created by Rajavelu Chandrasekaran on 09/09/15.
//  Copyright (c) 2015 Rajavelu Chandrasekaran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property (assign,nonatomic) int contactId;
@property (strong,nonatomic) NSString *name;
@property (strong,nonatomic) NSString *number;
@property (strong,nonatomic) NSString *changeDescription;

@end
